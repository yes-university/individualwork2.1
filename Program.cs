﻿using yesUniversity;

namespace IndividualWork2._1
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    1,
                    "Рядки"
                ),
                "Самостійна робота"
            );
            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
            {
                new("1. Реверс рядка", () =>
                {
                    Console.WriteLine("Введіть рядок:");
                    var line = Console.ReadLine();
                    Console.WriteLine(
                        "Результат: " +
                        string.Join(
                            "",
                            line?.ToCharArray().Reverse() ??
                            Array.Empty<char>()
                        )
                    );

                    return null;
                }),
                new("2. Капіталізація тексту", () =>
                {
                    Console.WriteLine("Введіть текст:");
                    var text = Console.ReadLine();
                    var words = text?.Split(' ') ?? Array.Empty<string>();
                    Console.WriteLine(
                        "Результат: " +
                        string.Join(" ", words.Select(word =>
                        {
                            var firstLetter = word[0];
                            var rest = word.Substring(1);
                            return firstLetter.ToString().ToUpper() + rest;
                        })));

                    return null;
                }),
                new("3. Паліндром", () =>
                {
                    // check if the line is a palindrome
                    Console.WriteLine("Введіть рядок:");
                    var line = (Console.ReadLine())?.ToLower();
                    var reversedLine = string.Join(
                        "",
                        line?.ToCharArray().Reverse() ??
                        Array.Empty<char>()
                    );
                    Console.WriteLine(  line == reversedLine ? "Рядок є паліндромом" : "Рядок не є паліндромом" );
                    return null;
                }),
            });
        }
    }
}